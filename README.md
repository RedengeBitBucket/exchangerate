# Exchange rate

Modul pro přepočet měn

## Instalace

1) Závislosti nainstalujeme pomocí [Composer](https://getcomposer.org):

```bash
composer require redenge/exchangerate
```
> V composer.json projektu je potřeba mít `"repositories": [{"type": "composer", "url": "https://satis.redenge.biz"}]`

2) Povolíme zápis do složky `temp` v rootu balíčku
```bash
chmod a+rw vendor/redenge/exchangerate/temp
```

## Použití
```php
$exchangeRate = new \Redenge\ExchangeRate\ExchangeRate;
$rate = $exchangeRate->getRate('EUR');
```

## Požadavky

- PHP: >= 5.5
- guzzlehttp/guzzle: ~6.2
- nette/safe-stream: "~2.3"
