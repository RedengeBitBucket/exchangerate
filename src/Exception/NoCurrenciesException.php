<?php

namespace Redenge\ExchangeRate\Exception;


/**
 * Description of NoCurrenciesException
 *
 * @author Michal Smejkal
 */
class NoCurrenciesException extends \Exception
{

}
