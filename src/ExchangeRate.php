<?php

namespace Redenge\ExchangeRate;

use DateTime;
use GuzzleHttp\Client;
use InvalidArgumentException;
use Nette\Utils\SafeStream;


/**
 * Description of ExchangeRate
 *
 * @author Michal Smejkal
 */
class ExchangeRate
{

	const
		CNB_URL = 'http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt',
		CACHE_FILEPATH_DEFAULT = __DIR__ . '/../temp/ExchangeRates.cache',
		EXPIRE_DATE_FORMAT = 'Y-m-d H:i:s';

	/**
	 * @var string
	 */
	private $cacheFilePath;

	/**
	 * @var array|NULL
	 */
	private $currencies;


	public function __construct()
	{
	}


	/**
	 * Nastavení cesty pro vytvoření cache souboru
	 *
	 * @param string $path
	 */
	private function setCacheFilePath($path)
	{
		$this->cacheFilePath = $path;
	}


	/**
	 * Vrátí kurz podle ČNB
	 *
	 * @param string $currencyIso
	 * @return float
	 */
	public function getRate($currencyIso)
	{
		$currencyIso = strtoupper($currencyIso);
		if (empty($this->currencies)) {
			$this->currencies = $this->getCurrencies();
		}
		if (empty($this->currencies)) {
			throw new Exception\NoCurrenciesException('Nepovedlo se načíst kurzy měn. '
				. 'Online služba pro stažení není aktuálně dostupná a offline kurzy neexistují.');
		}
		if (!isset($this->currencies[$currencyIso])) {
			throw new InvalidArgumentException('CURRENCY supported values: '
			. implode(", ", array_keys($this->currencies)) . ' given: ' . $currencyIso);
		}

		return $this->currencies[$currencyIso]->getRate();
	}


	/**
	 * Rozparsuje převody měn z url odpovědi do pole
	 *
	 * @param string $body
	 * @return array
	 */
	private function parseResponse($body)
	{
		$currencies = [];

		$rows = explode("\n", $body);
		unset($rows[0], $rows[1]);

		foreach ($rows as $row) {
			$row = trim($row);
			if (empty($row)) {
				continue;
			}
			list(, ,$amount, $currencyIso, $rate) = explode('|', $row);
			$rate = floatval(str_replace(',', '.', $rate)) / (int) $amount;
			$currencies[$currencyIso] = new Currency($currencyIso, $rate);
		}

		return $currencies;
	}


	/**
	 *
	 * @return array|NULL
	 */
	private function getOnlineCurrencies()
	{
		$options = [
			'verify' => FALSE,
		];
		$client = new Client;
		$response = $client->request('POST', self::CNB_URL, $options);
		if ($response->getStatusCode() !== 200) {
			return;
		}
		$body = (string) $response->getBody();

		return $this->parseResponse($body);
	}


	/**
	 * Vrátí kurzy měn
	 *
	 * @return void
	 */
	private function getCurrencies()
	{
		$cache = $this->getCurrenciesFromCache();
		if ($cache) {
			return $cache;
		}

		$currencies = $this->getOnlineCurrencies();
		if (!is_array($currencies)) {
			return $this->getCurrenciesFromCache(TRUE);
		}

		$data = [];
		$data['expire'] = (new DateTime)->modify('+8 hour')->format(self::EXPIRE_DATE_FORMAT);
		$data['currencies'] = $currencies;
		$dataSerialized = serialize($data);

		SafeStream::register();
		$cacheFilePath = 'nette.safe://' . ($this->cacheFilePath ? $this->cacheFilePath : self::CACHE_FILEPATH_DEFAULT);
		@file_put_contents($cacheFilePath, $dataSerialized);
		@chmod($cacheFilePath, 0777);

		$this->currencies = $data['currencies'];

		return $this->currencies;
	}


	/**
	 * Pokus o vrácení kurzu měn z cache
	 *
	 * @param bool $forceLoad - vynucení načtení z cache pro případ, když nebudou dostupné kurzy z webu
	 * @return array
	 */
	private function getCurrenciesFromCache($forceLoad = FALSE)
	{
		SafeStream::register();
		$cacheFilePath = 'nette.safe://' . ($this->cacheFilePath ? $this->cacheFilePath : self::CACHE_FILEPATH_DEFAULT);
		$content = @file_get_contents($cacheFilePath);
		if ($content === FALSE) {
			return NULL;
		}
		$data = unserialize($content);
		if ($data === FALSE) {
			return NULL;
		}

		$expire = isset($data['expire']) ? (new DateTime)->createFromFormat(self::EXPIRE_DATE_FORMAT, $data['expire']) : NULL;
		if ($expire === NULL || $expire instanceof DateTime === FALSE) {
			return NULL;
		}
		if ($expire < (new DateTime) && $forceLoad === FALSE) {
			return NULL;
		}

		$this->currencies = isset($data['currencies']) ? $data['currencies'] : NULL;

		return $this->currencies;
	}

}
