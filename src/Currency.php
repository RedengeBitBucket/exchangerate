<?php

namespace Redenge\ExchangeRate;


class Currency
{

	/**
	 * @var string - kód měny podle normy ISO 4217
	 */
	private $code;

	/**
	 * @var float
	 */
	private $rate;


	/**
	 * @param string    $code
	 * @param float     $rate
	 */
	public function __construct($code, $rate)
	{
		$this->code = strtoupper($code);
		$this->rate = $rate;
	}


	/**
	 * @return float
	 */
	public function getRate()
	{
		return $this->rate;
	}

}
